package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    //RegArgsConstructor - creates a constructor that requires arguments in the background
    public void employeeTest() {
        //@Data - creates the getters and setters in the background using the Lombok plugin
        //@Nonnull - states this should be provided in the backgorund constructor
        Employee hannah = new Employee();
        Employee bob = new Employee();

        SalesEmployee victoria = new SalesEmployee();

        //Employ Hannah and Fire bob
        hannah.employ();
        bob.employ();
        bob.fire();

        //Employ Victoria and give her a BMW
        victoria.employ();
        victoria.changeCar("BMW");

        //Give Victoria and hannah pay raises
        victoria.givePayRise(3000);
        hannah.givePayRise(2000);


        System.out.println("Hannah employment state: " + hannah.isEmployed());
        System.out.println("Bob employment state: " + bob.isEmployed());
        System.out.println("Victoria's employment state: " + victoria.isEmployed());
        System.out.println("Victoria's car: " + victoria.getCar());
        System.out.println("Victoria's Salary: " + victoria.getSalary());
        System.out.println("Hannah's Salary: " + hannah.getSalary());
        System.out.println("Number of Employees: " + Employee.countEmployees());

    }
}
