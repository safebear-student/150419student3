package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data

public class LoginPageLocators {
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By loginButtonLocator = By.id("enter");

    //searching for field locator via xPath instead of id
    private By rejectLoginMessageLocator = By.xpath("//p[@id='rejectLogin']/b");
    private By rememberMeBoxLocator = By.xpath("//input[@name='remember']");



}
