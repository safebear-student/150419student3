package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    private By loginSuccessMessagelocator = By.xpath("//p/b");
    private By newToolButtonLocator = By.xpath("//form/button[@class='btn btn-primary  btn-md']");
    private By deletePostmanButtonLocator = By.xpath("//tr/child::td[.='Postman']/following-sibling::td[3]/a/span[@class='glyphicon glyphicon-trash']");
    private By deleteJMeterButtonLocator = By.xpath("//tr/child::td[.='JMeter']/following-sibling::td[3]/a[@id='remove']");

}
