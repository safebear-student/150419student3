package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators locators = new LoginPageLocators();
    @Getter
    private String expectedPageTitle ="Login Page";

    @NonNull
    WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle();
    }
    //
    public void enterUsername(String username){
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
    }
    //method to enter password
    public void enterPassword(String password){
        driver.findElement(locators.getPasswordLocator()).sendKeys(password);
    }
    //method to click submit box
    public void clickSubmitButton(){
        driver.findElement(locators.getLoginButtonLocator()).click();
    }
    //method to click remember me box
    public void clickRememberMeBox(){
        driver.findElement(locators.getRememberMeBoxLocator()).click();
    }
    //method to find the login failed message
    public String getFailedLoginMessage(){
        return driver.findElement(locators.getRejectLoginMessageLocator()).getText();
    }
    //method to see if failed login message is visible
    public boolean isFailedLoginMessageVisible(){
        return driver.findElement(locators.getRejectLoginMessageLocator()).isDisplayed();
    }
    //method to check if Remember me box is checked
    public boolean isRememberMeChecked(){
       return driver.findElement(locators.getRememberMeBoxLocator()).isEnabled();
    }
    //method to complete all login steps
    public void login(String username, String password) {
        enterUsername(username);
        enterPassword(password);
        clickSubmitButton();
    }

}
