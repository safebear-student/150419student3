package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;
//ReqArgs used to pick a driver already running rather than creating a new one each run
@Data
@RequiredArgsConstructor
public class ToolsPage {

    ToolsPageLocators locators = new ToolsPageLocators();
    private String expectedPageTitle = "Tools Page";

    @NonNull
    WebDriver driver;
    //method to check if login successful message is there
    public String getLoginSuccessMessage(){
        return driver.findElement(locators.getLoginSuccessMessagelocator()).getText();
    }
    //method to click New Tool button
    public void clickNewToolButton(){
        driver.findElement(locators.getNewToolButtonLocator()).click();
    }

    public void clickDeletePostmanButton(){
        driver.findElement(locators.getDeletePostmanButtonLocator()).click();
    }

    public String getPageTitle(){
       return driver.getTitle();
    }

}
