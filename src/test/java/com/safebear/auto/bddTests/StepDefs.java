package com.safebear.auto.bddTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    //Creates an instance of a web driver
    WebDriver driver;

    LoginPage loginPage;
    ToolsPage toolsPage;


    //@Before to be run before every test case instance
    @Before
    public void setUp() {
        //finds and starts the web driver
        driver = Utils.getDriver();

        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    public void tearDown() {
        //will try to sleep for 2 seconds (to make the browser opening visible for humans. if exception found skips
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //.quit closes the whole browser (.close only closes the current tab) and shuts down the driver
        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        //Step 1 Action: Open our web application in the browser
        driver.get(Utils.getUrl());
        //Step 1 Check
        Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle(), "We're not on the login page or the title has changed");

    }

    @When("^I submit login details for a '(.+)'$")
    public void i_submit_login_details_for_a_user(String userType) throws Throwable {
        switch (userType) {
            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickSubmitButton();
                break;
            case "invalidUser":
                loginPage.login("hacker", "incorrect");
                break;
            default:
                Assert.fail("UserType supplied is not accounted for or typed wrong");
                break;
        }
    }

    @Then("^I can see the following message '(.+)'$")
    public void i_can_see_the_following_message(String message) throws Throwable {
        //Below line will create a screenshot
        Utils.captureScreenShot(driver, Utils.generateScreenShotFileName());
        switch (message) {
            case "Username or Password is incorrect":
                Assert.assertEquals(loginPage.getPageTitle(), loginPage.getExpectedPageTitle(), "We're not on the login page or the title has changed");
                Assert.assertTrue(loginPage.getFailedLoginMessage().contains(message));
                break;
            case "Login Successful":
                Assert.assertEquals(toolsPage.getPageTitle(),toolsPage.getExpectedPageTitle(),"We're on the wrong page");
                Assert.assertTrue(toolsPage.getLoginSuccessMessage().equals(message));
                break;
            default:
                Assert.fail("Message doesn't exist in the feature file or switch statement");
                break;
        }

    }
}
