package com.safebear.auto.utils;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {
    // live URL - http://toolslist.safebear.co.uk:8080 - Currently running on local
    private static final String URL = System.getProperty("url","http://localhost:8080/");
    private static final String BROWSER = System.getProperty("browser","chrome");

    public static String getUrl(){
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        System.setProperty("webdriver.gecko.driver","src/test/resources/drivers/geckodriver.exe");
        System.setProperty("webdriver.ie.driver","src/test/resources/drivers/IEDriverServer.exe");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("window-size=1366,768");

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments("window-size=1366,768");

//        InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
//        internetExplorerOptions.manage().window(1366,768);

        switch (BROWSER){
            case "chrome":
                return new ChromeDriver(chromeOptions);
            case "firefox":
                return new FirefoxDriver(firefoxOptions);
            case "ie":
                return new InternetExplorerDriver();
            case "chromeHeadless":
                chromeOptions.addArguments("headless","disable-gpu");
                return new ChromeDriver(chromeOptions);
            case "firefoxHeadless":
                firefoxOptions.addArguments("--headless");
                return new FirefoxDriver(firefoxOptions);
            default:
                return new ChromeDriver(chromeOptions);
        }

    }

    //code below to capture screenshots
    public static String generateScreenShotFileName(){
        //create filename
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
    }

    public static void captureScreenShot(WebDriver driver, String fileName){
        //take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        //make sure that the Screenshots directory exists
        File file = new File("target/screenshots");
                if(!file.exists()){
                    if(file.mkdir()){
                        System.out.println("Directory is Created!");
                    }else {
                        System.out.println("Failed to create directory");
                    }
                }

                //Copy file to filename and location we set before
        try {
            copy(scrFile, new File("target/screenshots/" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
