package com.safebear.auto.autoTests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest{

    @Test
    public void loginAsValidUser(){
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickSubmitButton();
        Assert.assertEquals(toolsPage.getPageTitle(),toolsPage.getExpectedPageTitle());
    }

    @Test
    public void loginAsInvalidUserAndCheckMessage(){
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
        loginPage.enterUsername("hacker");
        loginPage.enterPassword("letmein");
        loginPage.clickSubmitButton();
        Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
    }

}
