package com.safebear.auto.autoTests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {

    WebDriver driver;

    LoginPage loginPage;
    ToolsPage toolsPage;


    //@Before to be run before every test case instance
    @BeforeClass
    public void setUp() {
        //finds and starts the web driver
        driver = Utils.getDriver();

        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @AfterClass
    public void tearDown() {
        //will try to sleep for 2 seconds (to make the browser opening visible for humans. if exception found skips
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //.quit closes the whole browser (.close only closes the current tab) and shuts down the driver
        driver.quit();
    }

}
