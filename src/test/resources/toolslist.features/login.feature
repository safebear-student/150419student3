Feature: Login
  In order to Access the website
  As a User
  I want to know if my login is successful

  Rules:
  - The User must be informed if the login information is incorrect
  - The User must be informed if the login is successful

  Glossary:
  - User: Someone who wants to create a Tools List using our application
  - Supporters: This is what the customer calls 'Admin' users.

  Questions:
  - Do Users get locked out with too many attempts?

  @HighRisk
  @HighImpact
  Scenario Outline: A User logs into the application
    Given I navigate to the login page
    When I submit login details for a '<userType>'
    Then I can see the following message '<validationMessage>'
    Examples:
      | userType    | validationMessage                 |
      | invalidUser | Username or Password is incorrect |
      | validUser   | Login Successful                  |