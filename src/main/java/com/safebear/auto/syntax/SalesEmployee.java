package com.safebear.auto.syntax;

public class SalesEmployee extends Employee{

   private String car;

    public void changeCar(String newCar){
        car = newCar;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }
}
