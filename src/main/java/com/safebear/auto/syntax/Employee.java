package com.safebear.auto.syntax;

public class Employee {
    private boolean employed;
    private int salary;
    public static int numEmployees;

    public void fire(){
        employed = false;
        this.numEmployees --;
    }

    public void employ(){
        employed = true;
        this.numEmployees ++;
    }

    public void givePayRise(int payRiseAmount){
        salary += payRiseAmount;
    }

    public boolean isEmployed() {
        return employed;
    }

    public void setEmployed(boolean employed) {
        this.employed = employed;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public static int countEmployees(){
        return numEmployees;
    }
}
